import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import {reducer as formReducer} from 'redux-form';

import ids from './ids.js';
import concerns from './concerns.js';
import state from './state.js';


const  allReducers = combineReducers ({
    id: ids,
    concerns: concerns,
    state:state,
    form: formReducer,
	routing: routerReducer,
});

export default allReducers
