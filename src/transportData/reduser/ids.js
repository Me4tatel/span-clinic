const SET_ID = "SET_ID";
const CLEAR_ID = "CLEAR_ID";

export default function (state = null, action){
	switch (action.type) {
		case SET_ID:{
			let newObj = ( action.payload );
			return newObj;
		}

		case CLEAR_ID: {
            return null;
		}

		default:
			return state
	}
}
