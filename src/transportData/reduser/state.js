const LOGIN_LOADING = "LOGIN_LOADING";
const LOGIN_LOADING_SUCCESS = "LOGIN_LOADING_SUCCESS";
const LOGIN_LOADING_ERROR = "LOGIN_LOADING_ERROR";
const SEND_LOADING = "SEND_LOADING";
const SEND_SUCCESS = "SEND_SUCCESS";
const CLEAR_STATE = "CLEAR_STATE";

export default function (state = 0, action){
	switch (action.type) {
        case CLEAR_STATE:{
            return "CLEAR_STATE";
        }
		case LOGIN_LOADING:{
			return "LOGIN_LOADING";
		}
        case LOGIN_LOADING_SUCCESS:{
            return "LOGIN_LOADING_SUCCESS";
        }
        case LOGIN_LOADING_ERROR:{
            return "LOGIN_LOADING_ERROR";
        }
        case SEND_LOADING:{
            return "SEND_LOADING";
        }
        case SEND_SUCCESS: {
            return "SEND_SUCCESS";
        }

		default:
			return state
	}
}
