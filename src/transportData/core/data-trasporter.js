import * as axios from 'axios';

let dataTrasporter = axios.create({
    baseURL: 'https://snapclinic.azurewebsites.net/api/',
});

export default dataTrasporter;