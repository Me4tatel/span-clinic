const dataTrasporter = require('../core/data-trasporter.js').default;

export const getConcerns = () => {
    return dataTrasporter.get('Concerns')
        .then(result=>{
            if(result.error){
                throw new Error("Error in downloading objects");
            }
            return {
                type: 'GET_CONCERNS',
                payload: result.data.data
            };
        })
        .catch(error=>{
            console.log(error);
        });
};

export const getConcernSymptoms = (id) =>{
    return dataTrasporter.get(`ConcernSymptoms/${id}`)
        .then(result=>{
            if(result.error){
                throw new Error("Error in downloading objects");
            }
            return {
                type: 'DOWLOAD_CONCERN_SYMPTOMS',
                payload: result.data.data
            };
        })
        .catch(error=>{
            console.log(error);
        });
}