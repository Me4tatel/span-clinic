import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware, syncHistoryWithStore } from 'react-router-redux';
import { BrowserRouter } from 'react-router-dom';

import App from './components/Router';

import allReducers from './transportData/reduser';
import browserHistory from './history';

import 'react-input-range/lib/css/index.css';
import './view/css/libs.min.css';
import './view/css/main.less';

const promiseMiddleware = require('redux-promise').default;
const thunkMiddleware = require('redux-thunk').default;
const store = createStore(
	allReducers,
	composeWithDevTools(applyMiddleware(promiseMiddleware, thunkMiddleware, routerMiddleware(browserHistory)))
);

syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
