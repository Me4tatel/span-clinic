import React, {Component, Fragment} from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Scrollbar from 'react-scrollbars-custom';
import InputRange from 'react-input-range';
import ReactCountdownClock from 'react-countdown-clock';

import { getConcernSymptoms } from '../../transportData/actions/concerns';
import { sendResult } from '../../transportData/actions/results';
import { startSending } from '../../transportData/actions/state';

import logo from '../../view/img/logo.svg';
import arrowLeft from '../../view/img/icons/icon-arrow-left.svg';
import arrowRight from '../../view/img/icons/icon-arrow-right.svg';
import arrowRightGreen from '../../view/img/icons/icon-arrow-right-green.svg';
import arrowTop from '../../view/img/icons/icon-arrow-top.svg';
import arrowBottom from '../../view/img/icons/icon-arrow-bottom.svg';


class Register extends Component{
    constructor(props){
        super(props);
        this.state = {
            answers:{},
            value: 0,
            withError:false,
            withErrorList: [],
            scroll:{},
            scrollTop:0,
            showMessage: false,
        };
        this.content = React.createRef();
        this.props.getConcernSymptoms(this.props.match.params.id);
        this.submit = this.submit.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
        this.myRef = React.createRef();
        this.startTimeout();
        this.checkAnyEvent = this.checkAnyEvent.bind(this);
        this.hideMessage = this.hideMessage.bind(this);
    }

    timeoutMessage = {}
    checkAnyEvent(){
        clearTimeout(this.timeoutMessage);
        this.startTimeout();
    }

    startTimeout(){
        this.timeoutMessage = setTimeout(()=>{
            this.showMessage();
        }, 60000)
    }

    showMessage(){
        this.setState({
            showMessage: true
        })
    }

    handleScroll(props){
        this.setState({
            scroll: props
        })
    }

    componentDidUpdate(){
        if(this.props.state === "SEND_SUCCESS"){
            this.props.history.push("/thanks");
        }
    }

    arrPrevScroll = [];
    scrollTo(direction){
        let { scroll } = this.state;
        if(direction === "top"){
            this.arrPrevScroll.forEach((item)=>clearTimeout(item));
            this.arrPrevScroll = [];
            let scrollTop = scroll.scrollTop - 100 >= 0 ? scroll.scrollTop - 100 : 0;
            let different = scroll.scrollTop - scrollTop;
            if(different > 0){
                for(let i = 0; i<different; i+=5){
                    this.arrPrevScroll.push(setTimeout(() => (this.setState({
                        scrollTop: scroll.scrollTop - i
                    })), i * 2));
                }
            }
        } else {
            this.arrPrevScroll.forEach((item)=>clearTimeout(item));
            this.arrPrevScroll = [];

            let scrollTop = scroll.scrollTop + 100 + scroll.clientHeight <= scroll.scrollHeight  ? scroll.scrollTop + 100 : scroll.scrollHeight - scroll.clientHeight;
            let different = scrollTop - scroll.scrollTop;

            if(different > 0){
                for(let i = 0; i<different; i+=5){
                    this.arrPrevScroll.push(setTimeout(() => (this.setState({
                        scrollTop: scroll.scrollTop + i
                    })), i * 2));
                }
            }
        }
    }

    setValue(id, value, defaultValue){
        this.checkAnyEvent();
        let { answers } = this.state;
        answers[id] = value;
        if(value === defaultValue){
            let { withErrorList } = this.state;
            withErrorList.find((item) => (item.id === id)).value = true;
            this.setState({
                withErrorList
            })
        }else{
            let { withErrorList } = this.state;
            withErrorList.find((item) => (item.id === id)).value = false;
            this.setState({
                withErrorList
            })
        }
        this.setState({
            answers
        })
    }
    hideMessage(){
        this.setState({
            showMessage: false
        })
    }
    submit(){
        this.setState({
            withError:true
        });
        let data = {
            personalNumber: this.props.id,
            concernId:this.props.match.params.id,
            patientSymptomsViewModel: [],
        };
        if(this.state.withErrorList.map(item=>(item.value)).indexOf(true) === -1){
            let { answers } = this.state;
            for(let symptom in answers){
                data.patientSymptomsViewModel.push({
                    symptomId: symptom,
                    patientResponse: answers[symptom]
                })
            }
            this.props.sendResult(data);
        }else{
            let flagFirst = true;
            let el;
            this.state.withErrorList.forEach((item)=>{
                if(flagFirst && item.value === true){
                    el = item.el;
                    console.log(this.state.withErrorList, item);
                    flagFirst = false;
                }
            });
            let { scroll } = this.state;
            this.arrPrevScroll.forEach((item)=>clearTimeout(item));
            this.arrPrevScroll = [];
            console.log(scroll.scrollTop, el.current.offsetTop, this.content.current.offsetTop);
            let different = scroll.scrollTop - (el.current.offsetTop - this.content.current.offsetTop);
            console.log(different);
            if(different > 0) {
                for(let i = 0; i<different; i+=5){
                    this.arrPrevScroll.push(setTimeout(() => (this.setState({
                        scrollTop: scroll.scrollTop - i
                    })), i * 2));
                }
            } else{
                for(let i = 0; i<-different; i+=5){
                    this.arrPrevScroll.push(setTimeout(() => (this.setState({
                        scrollTop: scroll.scrollTop + i
                    })), i * 2));
                }
            }
        }
    }

    generateElement(symptom){
        switch(symptom.inputType){
            case 'CheckBox':
            case 'RadioButton':{
                if(typeof this.state.answers[symptom.id] === "undefined"){
                    let { withErrorList } = this.state;
                    withErrorList.push({id:symptom.id, value:true, el: React.createRef()});
                    this.setState({
                        withErrorList
                    })
                    this.setValue(symptom.id, '', '');
                }
                return(
                    <div className={"question" + (this.state.answers[symptom.id] === ''?" error":"")} ref={this.state.withErrorList.find((item) => (item.id === symptom.id)).el}>
                        <label>
                            <input type="radio" name={'question' + symptom.id} value="yes" onClick={()=>this.setValue(symptom.id, true, "")}/>
                            <span>YES</span>
                        </label>
                        <label>
                            <input type="radio" name={'question' + symptom.id} value="no" onClick={()=>this.setValue(symptom.id, false, "")}/>
                            <span>No</span>
                        </label>
                        <div className="error-message">Please give your answer to this question</div>
                    </div>
                )
            }
            case 'Scroller':{
                if(typeof this.state.answers[symptom.id] === "undefined"){
                    let { withErrorList } = this.state;
                    withErrorList.push({id:symptom.id, value:true, el: React.createRef()});
                    this.setState({
                        withErrorList
                    });
                    this.setValue(symptom.id, -1, -1);
                }
                let defValue = typeof this.state.answers[symptom.id] === "undefined" ? -1 : this.state.answers[symptom.id];
                return(
                    <div className={"question" + (this.state.answers[symptom.id] === -1?" error":" actived")} ref={typeof this.state.withErrorList.find((item) => (item.id === symptom.id)) !== "undefined" ?this.state.withErrorList.find((item) => (item.id === symptom.id)).el:this.myRef}>
                        <InputRange
                            maxValue={42}
                            minValue={0}
                            value={defValue}
                            onChange={value => { console.log(value); this.setValue( symptom.id, value, -1)}}
                            onChangeStart={defValue === -1 ?()=>this.setValue( symptom.id, 0, -1):()=>(0)}/>
                        <div className="btn minus"
                            onClick={() => { this.setValue( symptom.id, this.state.answers[symptom.id]>0? this.state.answers[symptom.id] - 1: 0, -1 )}}>
                            <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 12.9 26.4">
                                <path className="st0" d="M12.2,25.7L1.9,15.4c-1.2-1.2-1.2-3.2,0-4.4L12.2,0.7"/>
                            </svg>
                        </div>
                        <div className="btn plus"
                             onClick={() => { this.setValue( symptom.id, this.state.answers[symptom.id] === 42? 42: this.state.answers[symptom.id] + 1, -1)}}>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12.91 26.41">
                                <path className="cls-1" d="M.71.71,11,11a3.13,3.13,0,0,1,0,4.42L.71,25.71"/>
                            </svg>
                        </div>
                        <div className="error-message">Please take answer for this question</div>
                    </div>
                )
            }
            case 'Textarea':{
                if(typeof this.state.answers[symptom.id] === "undefined"){
                    let { withErrorList } = this.state;
                    withErrorList.push({id:symptom.id, value:true, el: React.createRef()});
                    this.setState({
                        withErrorList
                    })
                    this.setValue(symptom.id, '', '');
                }
                return(
                    <div className={"question" + (this.state.answers[symptom.id] === ''?" error":"")} ref={this.state.withErrorList.find((item) => (item.id === symptom.id)).el}>
                        <textarea
                            placeholder={symptom.placeHolder}
                            value={typeof this.state.answers[symptom.id] === "undefined"?'':this.state.answers[symptom.id]}
                            onChange={(event) => this.setValue(symptom.id, event.target.value, '')}
                        />
                        <div className="error-message">Please take answer for this question</div>
                    </div>
                );
            }
            case 'Textbox':{
                if(typeof this.state.answers[symptom.id] === "undefined"){
                    let { withErrorList } = this.state;
                    withErrorList.push({id:symptom.id, value:true, el: React.createRef()});
                    this.setState({
                        withErrorList
                    })
                    this.setValue(symptom.id, '', '');
                }
                return(
                    <div className={"question" + (this.state.answers[symptom.id] === ''?" error":"")} ref={this.state.withErrorList.find((item) => (item.id === symptom.id)).el}>
                        <input type="text" placeholder={symptom.placeHolder} value={typeof this.state.answers[symptom.id] === "undefined"?'':this.state.answers[symptom.id]} onChange={(event) => this.setValue(symptom.id, event.target.value, '')}/>
                        <div className="error-message">Please take answer for this question</div>
                    </div>
                );
            }
            default:{

            }
        }
    }

    moveBack(){
        this.props.clearState();
        this.props.history.push("/register")
    }

    render(){
        let name = '';
        let concern = {};
        let symptoms = [];
        if(this.props.concerns.length){
            concern = this.props.concerns.find((concern)=> String(concern.id) === String(this.props.match.params.id));
            symptoms = concern.symptoms;
            name = concern.name;
        }
        return (
            <div className={"page-choose" + (this.state.withError?" with-error": '')} onMouseMove={this.checkAnyEvent} onTouchStart={this.checkAnyEvent}>
                <header>
                    <div className="d-flex f-between f-align-center">
                        <div className="left">
                            <Link to="/concerns" className={"d-flex f-align-center"}>
                                <img src={arrowLeft} alt=""/>
                                <h2>{ name }</h2>
                            </Link>
                        </div>
                        <div to="/" className="right logo">
                            <img src={logo} alt=""/>
                        </div>
                    </div>
                </header>
                <div className="content wrapper-scrolled-area" ref={this.content}>
                    <Scrollbar
                        onScroll={this.handleScroll}
                        style={{width: '100%', height: '100%'}}
                        scrollTop={this.state.scrollTop}
                        trackYRenderer={
                            props => {
                                const {elementRef, ...restProps} = props;
                                return <span {...restProps}
                                             className="track"
                                             ref={elementRef}/>
                            }
                        }
                        thumbYRenderer={
                            props => {
                                const {elementRef, ...restProps} = props;
                                return(
                                    <Fragment>
                                        <div {...restProps} className="scroll-bar" ref={elementRef} />
                                        <div className="btn-to-top" onClick={()=>{this.scrollTo('top')}}>
                                            <img src={arrowTop} alt=""/>
                                        </div>
                                        <div className="btn-to-bottom" onClick={()=>{this.scrollTo('bottom')}}>
                                            <img src={arrowBottom} alt=""/>
                                        </div>
                                    </Fragment>
                                )
                            }
                        }>
                        <div className="symptoms d-flex f-wrap">
                            {symptoms && symptoms.map((symptom) => (
                                <div className="symptom d-flex" key={symptom.id}>
                                    <div className="block-6 title">
                                        {symptom.text}
                                    </div>
                                    <div className="block-6">
                                        {this.generateElement(symptom)}
                                    </div>
                                </div>
                            ))}
                        </div>
                        <div className="submit-block d-flex f-between">
                            <div className="block-6"/>
                            <div className="block-6 center">
                                <button onClick={this.submit} className="btn">Submit <img src={arrowRight} alt=""/></button>
                            </div>
                        </div>
                    </Scrollbar>
                </div>
                {this.state.showMessage?
                    <div className="message-next-patient">
                        <ReactCountdownClock seconds={30}
                                             color="#5E9CA3"
                                             alpha={1}
                                             size={130}
                                             weight={7}
                                             fontSize={"40px"}
                                             showMilliseconds={false}
                                             onComplete={this.moveBack} />
                        <h3>There was no action taken fot at least 1 min<br/> Are you still here?</h3>
                        <div className="btns d-flex">
                            <div className="block" onClick={this.hideMessage}>
                                <div className="btn"><img src={arrowLeft} alt=""/><span>Yes</span></div>
                            </div>
                            <div className="block" onClick={this.moveBack}>
                                <div className="btn white"><span>No, next patient</span><img src={arrowRightGreen} alt=""/></div>
                            </div>
                        </div>
                    </div>:''}
            </div>
        );
    }
}

export default withRouter(connect(
    (state, ownProps) => ({
        concerns: state.concerns,
        state: state.state,
        id: state.id,
        ownProps
    }),
    (dispatch) => ({
        getConcernSymptoms: (id) => {
            dispatch(getConcernSymptoms(id));
        },
        sendResult: (data) => {
            dispatch(startSending());
            dispatch(sendResult(data));
        },
        clearState: () => {
            dispatch({
                type:"CLEAR_STATE"
            })
        }
    })
)(Register));