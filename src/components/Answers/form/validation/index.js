export const requiredInput = (input) =>
    input ? undefined : `Please input value`;

export const onlyNumber = (input) =>
    !(/\D/.test(input)) ? undefined : `Please input only number`;