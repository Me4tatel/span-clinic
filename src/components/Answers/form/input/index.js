import React, {Fragment} from 'react';

export const CheckBox = props => {
    const {type, placeholder, meta} = props;
    return (
        <Fragment>
            <input {...props.input} type={type} placeholder={placeholder}/>

            {meta.error &&
            meta.touched &&
            <div className="error-message">
                {meta.error}
            </div>}
        </Fragment>
    );
};
export const RadioButton = CheckBox;

export const Scroller = props => {
    const {type, placeholder, meta} = props;
    return (
        <Fragment>
            <input {...props.input} type={type} placeholder={placeholder}/>

            {meta.error &&
            meta.touched &&
            <div className="error-message">
                {meta.error}
            </div>}
        </Fragment>
    );
};
export const Textbox = props => {
    const {type, placeholder, meta} = props;
    return (
        <Fragment>
            <input {...props.input} type={type} placeholder={placeholder}/>
            {meta.error &&
            meta.touched &&
            <div className="error-message">
                {meta.error}
            </div>}
        </Fragment>
    );
};
export const Textarea = props => {
    const {type, placeholder, meta} = props;
    return (
        <Fragment>
            <textarea {...props.input} type={type} placeholder={placeholder} />
            {meta.error &&
            meta.touched &&
            <div className="error-message">
                {meta.error}
            </div>}
        </Fragment>
    );
};