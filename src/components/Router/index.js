import React, {Component} from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Redirect from "react-router/es/Redirect";

import Register from '../Register/';
import Concern from '../Concern/';
import Answers from '../Answers/';
import Thanks from '../Thanks/';

import { getConcerns } from '../../transportData/actions/concerns.js'


class Router extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <Switch>
                <Route path="/thanks" component={Thanks}/>
                <Route path="/register" component={Register} />
                <Route path="/concerns" component={Concern}/>
                <Route path="/concern/:id" component={Answers}/>
                <Route component={() => <Redirect to="/register" />}/>
            </Switch>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(Router));
