import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import RegisterForm from './form/';

import { setId } from '../../transportData/actions/ids.js'
import { startLoading } from '../../transportData/actions/state.js'
import {getConcerns} from "../../transportData/actions/concerns";

class RegisterFormContainer extends Component{
    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.props.getConcerns();
    }

    componentDidUpdate(){
        if(!(this.props.id === null && typeof this.props.id === "object")){
            this.props.history.push('/concerns');
        }
    }

    submit = values => {
        this.props.setId(values.id);
    };

    render(){
        return (
           <div className="page-register">
               <div className="container">
                   <div className="d-flex f-center">
                       <RegisterForm onSubmit={this.submit}/>
                   </div>
               </div>
           </div>
        );
    }
};



export default withRouter(connect(
    (state, ownProps) => ({
        ownProps,
        id: state.id
    }),
    (dispatch) => ({
        setId: (id) => {
            dispatch(setId(id));
            dispatch(startLoading());
        },
        getConcerns: () => {
            dispatch(getConcerns())
        }
    })
)(RegisterFormContainer));