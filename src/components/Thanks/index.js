import React, {Component} from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { getConcernSymptoms } from '../../transportData/actions/concerns';

import logo from '../../view/img/logo.svg';
import arrowRight from '../../view/img/icons/icon-arrow-right.svg';

class Thanks extends Component{
    render(){
        return (
            <div className="page-choose">
                <header>
                    <div className="d-flex f-between f-align-center">
                        <div className="left">
                        </div>
                        <Link to="/" className="right logo">
                            <img src={logo} alt=""/>
                        </Link>
                    </div>
                </header>
                <div className="content">
                    <div className="thanks-page">
                        <h2>Your application was successfully saved.<br/>
                            It will be reviewed shortly.</h2>
                        <Link to="/register" onClick={this.props.clearResult()} className="btn">Next patient <img src={arrowRight} alt=""/></Link>
                    </div>
                </div>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        concerns: state.concerns,
        ownProps
    }),
    (dispatch) => ({
        getConcernSymptoms: (id) => {
            dispatch(getConcernSymptoms(id));
        },
        clearResult: () =>{
            dispatch({
                type: 'CLEAR_ID',
            });
        }
    })
)(Thanks));